#include <bits/stdc++.h>
#include<cstdlib>
using namespace std;
int main(){
    int hour1, hour2, mint1, mint2, inMin1, inMin2, distance;
    scanf("%d:%d\n%d:%d" ,&hour1, &mint1, &hour2, &mint2);
    inMin1 = hour1*60 + mint1;
    inMin2 = hour2*60 + mint2;
    distance = (inMin2 - inMin1)/2;
    int finAns = inMin1 + distance;
    printf("%02d:%02d\n",finAns/60,finAns%60);
    return 0;
}
