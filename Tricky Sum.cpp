#include <bits/stdc++.h>
#include<math.h>
using namespace std;

long long howMuchPT(long long n)
{
    long long counter = 0;
    while(n >= 2){
        n = n>>1;
        counter++;
    }
    return counter;
}

int main(){
    int ct;
    cin >> ct;
    long long sum = 0;
    for(int i=0; i<ct; i++){
        long long num; cin >> num;
        long long howMuch = howMuchPT(num);
        howMuch++;
        sum = ((num*(num+1))/2) - (2*(pow(2,howMuch)-1));
        cout << sum << endl;
    }
}
