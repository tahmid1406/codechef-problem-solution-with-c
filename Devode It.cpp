#include<bits/stdc++.h>
using namespace std;
int main(){
    int ct; cin >> ct;
    bool temp = true;
     while (ct--){
        long long num; cin >> num;
        long long counter = 0;

        while (num > 1){

            if(num % 5 == 0){
                num /= 5;
                num *= 4;
              }else if(num % 3 == 0){
                num /= 3;
                num *= 2;
              }else if(num % 2 == 0) {
                num /= 2;
              }else{
                counter = -1;
                break;
            }
            counter++;
        }
        if(temp) cout << counter << endl;
    }


}
