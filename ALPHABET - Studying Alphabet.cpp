#include <bits/stdc++.h>
#include<iostream>
using namespace std;
int main (){
    string s; cin >> s;
    int arrOfChar[26] = {0};
    int len = (int)s.size();
    for(int i=0; i<len; i++){
        char ascii = s[i];
        int index = int(ascii);
        index = index - 97;
        arrOfChar[index] = 1;
    }
    int ct; cin >> ct;
    for(int i=0; i<ct; i++){
        string temp; cin >> temp;
        int counter = 0;
        for(int j=0; j<(int)temp.size(); j++){
            char x = temp[j];
            int index = int(x);
            index -= 97;
            if(arrOfChar[index] == 0) counter++;
        }
        if(counter > 0) cout << "No" << endl;
        else cout << "Yes" << endl;
    }

}



