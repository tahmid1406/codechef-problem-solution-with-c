#include<bits/stdc++.h>
using namespace std;
int main(){
    int n; cin >> n;
    int arr[n][n];
    int ans = 0;
    char coder = 'C';
    char dot = '.';
    if(n %2 == 0){
        ans = (n/2)*n;
    }else{
        ans=((n+1)/2)*((n+1)/2);
        ans+=(n/2)*(n/2);

    }
    cout << ans << endl;
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            if(i%2==0){
                if(j%2==0){
                    cout << coder;
                }else cout << dot;
            }else{
                if(j%2==0) cout << dot;
                else cout << coder;
            }
        }
        cout << endl;
    }
    return 0;
}
