#include <bits/stdc++.h>
#include<string>
using namespace std;
int main()
{
    string s; cin >> s;
    int tot = s.length();
    int aCnt=0, oCnt=0;
    for(int i=0; i<tot; i++){
        if(s[i]=='a') aCnt++;
        else oCnt++;
    }
    if(aCnt > oCnt){
        cout << tot << endl;
    }else if(aCnt == oCnt){
        cout << tot-1 << endl;
    }else{
        int temp = 0;
        while(oCnt--){
                temp++;
            if(aCnt > oCnt) break;
        }
        cout << tot - temp << endl;
    }
    return 0;
}
