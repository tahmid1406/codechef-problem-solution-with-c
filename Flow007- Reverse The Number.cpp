#include<iostream>
using namespace std;
int main (){
    int ct;
    cin >> ct;
    for (int i=0; i<ct; i++){
        int number;
        cin >> number;
        int sum = 0;
        int revNum = 0;
        while(number != 0){
            int rem = number % 10;
            revNum = (revNum *10) + rem;
            number /= 10;
        }
        cout << revNum << endl;
    }
}
