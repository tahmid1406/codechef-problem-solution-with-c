
#include<iostream>
using namespace std;
int digitSum (int n){
    int sum = 0;
    while(n != 0){
        int rem = n%10;
        sum += rem;
        n /= 10;
    }
    return sum;
}
int main (){
    int ct;
    cin >> ct;
    for (int i=0; i<ct; i++){
        int number;
        cin >> number;
        int sum = digitSum(number);
        cout << sum << endl;
    }
}
