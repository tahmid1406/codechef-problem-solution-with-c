#include<cstdio>
#include<iostream>
#include<string>
#include <cctype>
using namespace std;
int main(){
    int ct;
    cin >> ct;
    while(ct--){
        int n,k; cin >> n >> k;
        string s; cin >> s;
        int upperCount = 0;
        int lowerCount = 0;
        for(int i=0; i<n; i++){
            if(isupper(s[i])) upperCount++;
            else lowerCount++;
        }
        if(upperCount == lowerCount){
            if(k >= upperCount) cout << "both" << endl;
            else if(k < upperCount) cout << "none" << endl;
        }else if(upperCount > lowerCount){
            if(k < lowerCount) cout << "none" << endl;
            else if(k >= lowerCount && k < upperCount) cout << "brother" << endl;
            else if(k >= upperCount) cout << "both" << endl;
        }else if(upperCount < lowerCount){
            if(k < upperCount) cout << "none" << endl;
            else if(k >=upperCount && k < lowerCount) cout << "chef" << endl;
            else cout << "both" << endl;
        }
    }
}

