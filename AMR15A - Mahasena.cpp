#include <bits/stdc++.h>
#include<iostream>
using namespace std;
int main(){
    int ct; cin >> ct;
    int evenCount = 0, oddCount = 0;
    for(int i =0; i<ct; i++){
        int temp; cin >> temp;
        if(temp % 2 == 0) evenCount++;
        else oddCount++;
    }
    if(evenCount > oddCount) cout << "READY FOR BATTLE" << endl;
    else cout << "NOT READY" << endl;
}

