#include<bits/stdc++.h>
using namespace std;
int main(){
    int ct; cin >> ct;
    for (int i=0; i<ct; i++){
        int n; cin >> n;
        int noat = 0;
        int div;
        if(n >= 100){
            div = n/100;
            noat += div;
            n %= 100;
        }
        if(n >= 50 && n < 100){
            div = n/50;
            noat += div;
            n %= 50;
        }
        if(n>=10 && n <50){
            div = n/10;
            noat += div;
            n %= 10;
        }
        if(n>=5 && n<10){
            div = n/5;
            noat += div;
            n %= 5;
        }
        if(n >= 2 && n<5){
            div = n/2;
            noat += div;
            n %= 2;
        }
        if(n == 1){
            noat ++;
        }
        cout << noat << endl;
    }
}
