#include <bits/stdc++.h>
using namespace std;
int main(){
    vector<int> numVec;
    int ct; cin>> ct;
    int counter = 0;
    for(int i=0; i<ct; i++){
        int temp; cin >> temp;
        numVec.push_back(temp);
    }

    for(int i=0; i<ct; i++){
        if(numVec.at(i) != i+1) counter ++;
    }

    if(counter <= 2) cout << "YES" << endl;
    else cout << "NO" << endl;
}
