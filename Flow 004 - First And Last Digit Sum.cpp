#include <bits/stdc++.h>
using namespace std;
int noOfDigits(int n){
    int counter = 0;
    while(n != 0){
        n/=10;
        counter++;
    }
    return counter;
}
int main()
{
    int ct; cin >> ct;
    for(int i=0; i<ct; i++){
        int n; cin >> n;
        int digit = noOfDigits(n);
        int firstAndLastSum = 0;
        int ctr = 1;
        while(n != 0){
            int rem = n%10;
            if(ctr == 1 || ctr == digit) firstAndLastSum += rem;
            n /= 10;
            ctr ++;
        }
        cout << firstAndLastSum << endl;
    }
    return 0;

}


