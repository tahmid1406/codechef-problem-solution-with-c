#include <bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin >> s;
    int pCount=0, sCount=0;
    for(int i=0; i<s.length(); i++){
        if(s[i] == 'o') sCount++;
        else if(s[i]=='-') pCount++;
    }
    if(sCount == 0) cout << "YES" << endl;
    else if(pCount % sCount == 0) cout << "YES" << endl;
    else cout << "NO" << endl;
}

